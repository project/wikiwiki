-- SUMMARY --
WikiWiki gives you one place to create, share and find
important information for your site.

-- FEATURES --
- Create, read and modify WikiWiki pages.

-- NO REQUIREMENTS --

-- INSTALLATION --
* To install the module copy the 'wikiwiki' folder to
   your sites/all/modules/contrib directory.
* Go to admin/build/modules. Enable the module.
   Read more about installing modules at http://drupal.org/node/70151
* Go to admin/wikiwiki for the homepage of WikiWiki.
* Users with permissions can have access to WikiWiki, manage the WikiWiki pages
   and manage the settings page.

-- CONFIGURATION --
* Configure user permissions in Administration -> People -> Permissions.
* This module uses the permissions:
  * 'acess wikiwiki';
  * 'manage pages wikiwiki';
  * 'manage settings wikiwiki'.
* If you want to let the users manage the Menu page you need to give them also
  the permission: 'administer menu'.

-- CONTACT --
Current maintainers:
* Gabriele Manna (geberele) - https://drupal.org/user/1183748

-- TODO --
* Submodule for comments.
* Submodule for revisions.
* Submodule for notifications.
