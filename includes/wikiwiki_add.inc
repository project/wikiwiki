<?php

/**
 * @file
 * Form to add a new WikiWiki page.
 */

/**
 * Callback for admin/wikiwiki/new.
 */
function wikiwiki_add() {
  $output = drupal_get_form('wikiwiki_add_form');
  return $output;
}

/**
 * Form - Create a new WikiWiki page.
 */
function wikiwiki_add_form(&$form_state) {
  $form['title'] = array(
    '#type' => 'textfield',
    '#title' => t('Title'),
    '#maxlength' => 60,
    '#default_value' => '',
    '#weight' => 0,
    '#required' => TRUE,
  );

  $form['body'] = array(
    '#type' => 'textarea',
    '#title' => t('Body'),
    '#default_value' => '',
    '#rows' => 20,
    '#weight' => 1,
    '#required' => TRUE,
  );

  $form['format'] = filter_form(FILTER_FORMAT_DEFAULT, 1);

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
    '#weight' => 2,
  );

  return $form;
}

/**
 * Submit Form - Create a new WikiWiki page.
 */
function wikiwiki_add_form_submit($form, &$form_state) {
  global $user;

  $uid = $user->uid;
  $title = $form_state['values']['title'];
  $body = $form_state['values']['body'];
  $format = $form_state['values']['format'];

  $record = new stdClass();
  $record->uid = $uid;
  $record->title = $title;
  $record->body = $body;
  $record->timestamp = time();
  $record->format = $format;

  if (drupal_write_record('wikiwiki', $record)) {
    $wid = $record->wid;

    module_load_include('inc', 'menu', 'menu.admin');

    $item = array(
      'link_path' => 'admin/wikiwiki/page/' . $wid,
      'mlid' => 0,
      'module' => 'menu',
      'has_children' => 0,
      'customized' => 1,
      'link_title' => $title,
      'description' => $title,
      'expanded' => 0,
      'parent' => 'wikiwiki:0',
      'weight' => 0,
      'hidden' => 0,
      'plid' => 0,
      'menu_name' => 'wikiwiki',
    );

    if (!menu_link_save($item)) {
      drupal_set_message(t('There was an error saving the menu link.'), 'error');
    }
    else {
      drupal_set_message(t('Data saved with success.'));
      drupal_goto('admin/wikiwiki/page/' . $wid);
    }
  }
}
