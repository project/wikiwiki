<?php

/**
 * @file
 * Content page.
 */

/**
 * Returns the Content page.
 *
 * @return string
 *   Returns a table output if there is content.
 */
function wikiwiki_content() {
  $header = array(
    0 => array('field' => 'wid', 'data' => 'Page ID'),
    1 => array('field' => 'uid', 'data' => 'User ID'),
    2 => array('field' => 'title', 'data' => 'Title'),
    3 => array('field' => 'timestamp', 'data' => 'Created', 'sort' => 'desc'),
    4 => array('field' => 'operations', 'data' => 'Operations'),
  );

  $element = 0;
  $limit = 20;
  $query = 'SELECT wid, uid, title, timestamp FROM {wikiwiki}';
  $count_query = 'SELECT COUNT(*) FROM (' . $query . ') AS count_query';
  $q = pager_query($query . tablesort_sql($header), $limit, $element, $count_query);
  $rows = array();

  while ($r = db_fetch_array($q)) {
    $row['wid'] = $r['wid'];
    $row['uid'] = $r['uid'];
    $row['title'] = $r['title'];
    $row['timestamp'] = format_date($r['timestamp']);
    $row['operations'] = l(t('Edit'), 'admin/wikiwiki/page/' . $r['wid'] . '/edit') . ' | ';
    $row['operations'] .= l(t('Delete'), 'admin/wikiwiki/page/' . $r['wid'] . '/delete');
    $rows[]['data'] = $row;
  }

  if (count($rows) > 0) {
    $output = theme('table', $header, $rows);
    $output .= theme('pager', NULL, $limit, $element);
  }
  else {
    $output = t('No WikiWiki content available.');
  }

  return $output;
}
