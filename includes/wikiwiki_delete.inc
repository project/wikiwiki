<?php

/**
 * @file
 * Confirmation page to delete a WikiWiki page.
 */

/**
 * Callback for admin/wikiwiki/page/%/delete.
 */
function wikiwiki_delete($wid) {
  if (wikiwiki__page_exists($wid)) {
    $output = drupal_get_form('wikiwiki_delete_confirm', $wid);
  }
  else {
    $output = 'WikiWiki page not found.';
  }
  return $output;
}

/**
 * Menu callback -- ask for confirmation of WikiWiki page deletion.
 */
function wikiwiki_delete_confirm(&$form_state, $wid) {
  $wikiwiki_node = wikiwiki__load_page($wid);
  $form['wid'] = array(
    '#type' => 'value',
    '#value' => $wid,
  );
  $desc = '<h2>' . t('Are you sure you want to delete %title?', array('%title' => $wikiwiki_node->title)) . '</h2>';
  $desc .= t('This action cannot be undone.');
  $path = 'admin/wikiwiki/page/' . $wid;
  return confirm_form($form, 'WikiWiki', $path, $desc, t('Delete'), t('Cancel'));
}

/**
 * Execute node deletion.
 */
function wikiwiki_delete_confirm_submit($form, &$form_state) {
  if ($form_state['values']['confirm']) {
    if (wikiwiki__delete_page($form_state['values']['wid'])) {
      drupal_set_message(t('Content deleted with success.'));
    }
    else {
      drupal_set_message(t('The content has not been deleted. The query failed.'), 'error');
    }
  }

  $form_state['redirect'] = 'admin/wikiwiki';
}
