<?php

/**
 * @file
 * Form to edit a WikiWiki page.
 */

/**
 * Callback for admin/wikiwiki/new.
 */
function wikiwiki_edit($wid) {
  if (wikiwiki__page_exists($wid)) {
    drupal_add_css(drupal_get_path('module', 'wikiwiki') . '/css/wikiwiki.css');
    $output = theme('wikiwiki_options', $wid);
    $output .= drupal_get_form('wikiwiki_edit_form', $wid);
  }
  else {
    $output = 'WikiWiki page not found.';
  }
  return $output;
}

/**
 * Form - Edit a WikiWiki page.
 */
function wikiwiki_edit_form(&$form_state, $wid) {

  $wikiwiki_node = wikiwiki__load_page($wid);
  $title = (isset($wikiwiki_node->title)) ? $wikiwiki_node->title : '';
  $body = (isset($wikiwiki_node->body)) ? $wikiwiki_node->body : '';
  $format = (isset($wikiwiki_node->format)) ? $wikiwiki_node->format : 0;

  $form['title'] = array(
    '#type' => 'textfield',
    '#title' => t('Title'),
    '#maxlength' => 60,
    '#default_value' => $title,
    '#weight' => 0,
    '#required' => TRUE,
  );

  $form['body'] = array(
    '#type' => 'textarea',
    '#title' => t('Body'),
    '#default_value' => $body,
    '#rows' => 20,
    '#weight' => 1,
    '#required' => TRUE,
  );

  $form['format'] = filter_form($format, 1);

  $form['wid'] = array(
    '#type' => 'hidden',
    '#value' => $wid,
  );

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
    '#weight' => 2,
  );

  return $form;
}

/**
 * Submit Form - Edit a WikiWiki page.
 */
function wikiwiki_edit_form_submit($form, &$form_state) {
  $wid = $form_state['values']['wid'];
  $wikiwiki_node = wikiwiki__load_page($wid);

  $title = $form_state['values']['title'];
  $body = $form_state['values']['body'];
  $format = $form_state['values']['format'];

  $wikiwiki_node->title = $title;
  $wikiwiki_node->body = $body;
  $wikiwiki_node->format = $format;

  if (drupal_write_record('wikiwiki', $wikiwiki_node, 'wid')) {
    drupal_set_message(t('Content saved with success.'));
    drupal_goto('admin/wikiwiki/page/' . $wid);
  }
  else {
    drupal_set_message(t('There was an error saving the WikiWiki page.'), 'error');
  }
}
