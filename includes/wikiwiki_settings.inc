<?php

/**
 * @file
 * Settings form.
 */

/**
 * Callback for the wikiwiki menu page.
 */
function wikiwiki_settings_menu_page() {
  // Load menu.admin.inc from the menu module.
  module_load_include('inc', 'menu', 'menu.admin');
  $menu_name = 'wikiwiki';
  $menu = db_fetch_array(db_query("SELECT * FROM {menu_custom} WHERE menu_name = '%s'", $menu_name));
  $form = drupal_get_form('menu_overview_form', $menu);
  return $form;
}

/**
 * Callback for the wikiwiki settings page.
 */
function wikiwiki_settings_page() {
  $form = drupal_get_form('wikiwiki_settings_form');
  return $form;
}

/**
 * Form - wikiwiki settings page.
 */
function wikiwiki_settings_form() {
  $settings = array(
    'hp_options' => 0,
    'hp_text' => '',
  );
  $settings = variable_get('wikiwiki_settings', $settings);
  $options = array(
    0 => 'Show only the default image',
    1 => 'Show only the custom text',
    2 => 'Show custom text after default image',
  );

  $form['hp_options'] = array(
    '#type' => 'radios',
    '#title' => t('WikiWiki Homepage'),
    '#options' => $options,
    '#default_value' => $settings['hp_options'],
    '#weight' => 0,
    '#required' => TRUE,
  );

  $form['hp_text'] = array(
    '#type' => 'textarea',
    '#title' => t('Custom text homepage'),
    '#default_value' => $settings['hp_text'],
    '#rows' => 10,
    '#weight' => 1,
  );

  $form['format'] = filter_form(FILTER_FORMAT_DEFAULT, 1);

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
    '#weight' => 2,
  );

  return $form;
}

/**
 * Submit Form - wikiwiki settings page.
 */
function wikiwiki_settings_form_submit($form, &$form_state) {
  $settings = array(
    'hp_options' => $form_state['values']['hp_options'],
    'hp_text' => $form_state['values']['hp_text'],
    'hp_format' => $form_state['values']['format'],
  );
  variable_set('wikiwiki_settings', $settings);
  drupal_set_message(t('Data saved with success.'));
}
