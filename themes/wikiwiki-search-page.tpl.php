<?php

/**
 * @file
 * Template for the search page.
 */
?>

<?php if ($rows != "") : ?>
  <?php  foreach ($rows as $row) : ?>
    <div class="wikiwiki-search-result-row">
      <?php echo $row['title']; ?>
      <div class="wikiwiki-search-result-row-teaser">
        <?php echo $row['teaser']; ?>
      </div>
    </div>
  <?php endforeach; ?>
  <?php echo $pager; ?>
<?php else : ?>
  <?php echo $noresults; ?>
<?php endif; ?>
