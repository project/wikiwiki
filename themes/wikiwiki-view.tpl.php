<?php

/**
 * @file
 * Template for the WikiWiki page.
 */
?>
<div id="wikiwiki">
  <div id="wikiwiki-left"><?php echo $wikiwiki_left; ?></div>
  <div id="wikiwiki-right"><?php echo $wikiwiki_right; ?></div>
</div>
